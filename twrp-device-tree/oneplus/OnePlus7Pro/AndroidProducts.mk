#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_OnePlus7Pro.mk

COMMON_LUNCH_CHOICES := \
    omni_OnePlus7Pro-user \
    omni_OnePlus7Pro-userdebug \
    omni_OnePlus7Pro-eng
