#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus7Pro.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus7Pro-user \
    lineage_OnePlus7Pro-userdebug \
    lineage_OnePlus7Pro-eng
