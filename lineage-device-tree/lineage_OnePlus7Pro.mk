#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from OnePlus7Pro device
$(call inherit-product, device/oneplus/OnePlus7Pro/device.mk)

PRODUCT_DEVICE := OnePlus7Pro
PRODUCT_NAME := lineage_OnePlus7Pro
PRODUCT_BRAND := OnePlus
PRODUCT_MODEL := GM1917
PRODUCT_MANUFACTURER := oneplus

PRODUCT_GMS_CLIENTID_BASE := android-oneplus

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="aospa_oneplus7pro-userdebug 12 SKQ1.220603.001 eng.master.20220624.105907 test-keys"

BUILD_FINGERPRINT := OnePlus/aospa_oneplus7pro/oneplus7pro:12/SKQ1.220603.001/master06241059:userdebug/test-keys
